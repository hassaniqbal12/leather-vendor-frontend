import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {NewpasswordModel} from './NewpasswordModel';
@Injectable({
  providedIn: 'root'
})
export class NewPasswordService {
  baseurl: string = "http://localhost:3000/api/rusers/";
  constructor(private http:HttpClient) { }
  newpassword(user: NewpasswordModel){
    return this.http.post(this.baseurl + 'reset-password', user);
  }
}
