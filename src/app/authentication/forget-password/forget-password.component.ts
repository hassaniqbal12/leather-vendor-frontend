import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { first } from "rxjs/operators";
import {NewPasswordService} from "./new-password.service";
@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {
  passwordform = this.fb.group({
    
    password: ['', Validators.required ],

    
  });
  password = new FormControl('');
  constructor(public fb:FormBuilder,private toastr: ToastrService,private router: Router,private newpasswordservice: NewPasswordService) { }

  ngOnInit() {
  }
  newpassword(){
    if(this.passwordform.valid){
      this.newpasswordservice.newpassword(this.passwordform.value)
      .subscribe( data => {
        console.log(data);
        this.toastr.success("Password Changed Sucessfully")
        this.router.navigate(['/profile']);
      });
    }
}
}
