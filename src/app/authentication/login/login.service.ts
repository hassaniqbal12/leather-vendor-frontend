import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {UserModel} from './usermodel';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  baseurl: string = "http://localhost:3000/api/rusers/";
  constructor(private http:HttpClient) { }
  adduser(user: UserModel){
    return this.http.post(this.baseurl + 'login', user);
  }
  // getUserById(id: number) {
  //   return this.http.get(this.baseurl + id);
  // }
}
