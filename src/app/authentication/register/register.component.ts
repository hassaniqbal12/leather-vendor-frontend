import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { RegistrationService } from './registration.service';
import { first } from "rxjs/operators";
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  
  constructor(public fb:FormBuilder,private toastr: ToastrService,private router: Router, private registerservice: RegistrationService) { }

  registerform = this.fb.group({
    email: ['', Validators.required ],
    password: ['', Validators.required],
    username: ['', Validators.required],
    city: ['', Validators.required],
    phoneno: ['', Validators.required],
  });
  ngOnInit() {

  }
  email = new FormControl('');
  password = new FormControl('');
  username = new FormControl('');
  city = new FormControl('');
  phoneno = new FormControl('');
 
  register(){
      if(this.registerform.valid){
        this.registerservice.adduser(this.registerform.value)
        .subscribe( data => {
          console.log(data);
          this.toastr.success("Register Sucessfully")
          this.router.navigate(['/profile']);
        });
      }
    
  }
}
