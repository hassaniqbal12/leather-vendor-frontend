import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { first } from "rxjs/operators";
import { ForgetemailModel} from './ForgetemailModel';
import { ForgetEmailService } from './forget-email.service';
@Component({
  selector: 'app-forget-email',
  templateUrl: './forget-email.component.html',
  styleUrls: ['./forget-email.component.css']
})
export class ForgetEmailComponent implements OnInit {
  emailform = this.fb.group({
    
    email: ['', Validators.required ],

    
  });
  constructor(public fb:FormBuilder,private toastr: ToastrService,private router: Router,private emailforgetsearch: ForgetEmailService) { }

  ngOnInit() {
  }
  emailuser(){
    if(this.emailform.valid){
      this.emailforgetsearch.addemail(this.emailform.value)
      .subscribe( data => {
        console.log(data);
        this.toastr.success("Email Got Sucessfully");
        this.router.navigate(['/forgetpassword']);
    });
    }
    else{

        this.toastr.success("Invalid Email");
      
    }

  }
}
