import { TestBed } from '@angular/core/testing';

import { ForgetEmailService } from './forget-email.service';

describe('ForgetEmailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ForgetEmailService = TestBed.get(ForgetEmailService);
    expect(service).toBeTruthy();
  });
});
