import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {ForgetemailModel} from './ForgetemailModel';
@Injectable({
  providedIn: 'root'
})
export class ForgetEmailService {
  baseurl: string = "http://localhost:3000/api/rusers/";
  constructor(private http:HttpClient) { }
  addemail(user: ForgetemailModel){
    return this.http.post(this.baseurl + 'reset', user);
  }
}
