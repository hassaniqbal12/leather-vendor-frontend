export class AdddriverModel {
    id: string;
    name: String;
    age: String;
    licenseimage: String;
    licenseno: number;
    carimage: string;
    created_at: Date;
    updated_at: Date;
}