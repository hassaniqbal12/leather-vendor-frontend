import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { AdddriverService } from './adddriver.service';
import { first } from "rxjs/operators";
@Component({
  selector: 'app-add-driver',
  templateUrl: './add-driver.component.html',
  styleUrls: ['./add-driver.component.css']
})
export class AddDriverComponent implements OnInit {

  constructor(public fb:FormBuilder,private toastr: ToastrService,private router: Router, private driverservice: AdddriverService) { }
  adddriverform = this.fb.group({
    name: ['', Validators.required ],
    licenseno: ['', Validators.required],
    age: ['', Validators.required],
    licenseimage: ['', Validators.required]
  });
  ngOnInit() {
  }
  name = new FormControl('');
  age = new FormControl('');
  licenseno = new FormControl('');
  licenseimage = new FormControl('');
 
  adddriver(){
      if(this.adddriverform.valid){
        this.driverservice.adddriver(this.adddriverform.value)
        .subscribe( data => {
          console.log(data);
          this.toastr.success("Driver Added Sucessfully")
          this.router.navigate(['/profile']);
        });
      }
  }
}
