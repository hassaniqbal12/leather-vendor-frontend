import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {AdddriverModel} from './AdddriverModel';
@Injectable({
  providedIn: 'root'
})
export class AdddriverService {

  baseurl: string = "http://localhost:3000/api/";
  constructor(private http:HttpClient) { }
  adddriver(adddriver: AdddriverModel){
    return this.http.post(this.baseurl + 'drivers', adddriver);
  }
}
