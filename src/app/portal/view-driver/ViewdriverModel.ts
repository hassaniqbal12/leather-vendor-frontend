export class ViewdriverModel {
    id: string;
    name: String;
    age: String;
    licenseimage: String;
    licenseno: number;
    created_at: Date;
    updated_at: Date;
}