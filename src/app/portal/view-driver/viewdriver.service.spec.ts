import { TestBed } from '@angular/core/testing';

import { ViewdriverService } from './viewdriver.service';

describe('ViewdriverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewdriverService = TestBed.get(ViewdriverService);
    expect(service).toBeTruthy();
  });
});
