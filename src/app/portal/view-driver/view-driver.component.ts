import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ViewdriverService} from './viewdriver.service';
@Component({
  selector: 'app-view-driver',
  templateUrl: './view-driver.component.html',
  styleUrls: ['./view-driver.component.css']
})
export class ViewDriverComponent implements OnInit {

  drivers = <any>[];
  constructor(private router: Router, private driverservice: ViewdriverService) { }

  ngOnInit() {
    this.viewalldriver();
  }
  viewalldriver() {
  
    this.driverservice.viewalldriver().subscribe((data: {}) => {
      console.log(data);
      this.drivers = data;
    });
  }

}
