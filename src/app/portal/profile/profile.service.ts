import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {ProfileModel} from './ProfileModel';
@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  baseurl: string = "http://localhost:3000/api/accounts-users/";
  constructor(private http:HttpClient) { }
  logout(profile: ProfileModel){
    return this.http.post(this.baseurl + 'logout', profile);
  }
}
