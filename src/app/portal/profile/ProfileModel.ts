export class ProfileModel {
    id: string;
    username: String;
    email: String;
    password: String;
    phoneno: number;
    city: string;
    userimage: String;
    usercnic: String;
    created_at: Date;
    updated_at: Date;
}