import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {AddcarModel} from './AddcarModel'
@Injectable({
  providedIn: 'root'
})
export class AddCarService {

  baseurl: string = "http://localhost:3000/api/";
  constructor(private http:HttpClient) { }
  addcar(addcar: AddcarModel){
    return this.http.post(this.baseurl + 'cars', addcar);
  }
}
