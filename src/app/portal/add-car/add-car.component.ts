import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { AddCarService } from './add-car.service';
import { first } from "rxjs/operators";
@Component({
  selector: 'app-add-car',
  templateUrl: './add-car.component.html',
  styleUrls: ['./add-car.component.css']
})
export class AddCarComponent implements OnInit {

  constructor(public fb:FormBuilder,private toastr: ToastrService,private router: Router, private addcarservice: AddCarService) { }

  addcarform = this.fb.group({
    name: ['', Validators.required ],
    model: ['', Validators.required],
    driven: ['', Validators.required],
    licenseno: ['', Validators.required],
    carimage: ['', Validators.required],
  });
  ngOnInit() {

  }
  name = new FormControl('');
  model = new FormControl('');
  driven = new FormControl('');
  licenseno = new FormControl('');
  carimage = new FormControl('');
 
  addcar(){
      if(this.addcarform.valid){
        this.addcarservice.addcar(this.addcarform.value)
        .subscribe( data => {
          console.log(data);
          this.toastr.success("Car Added Sucessfully")
          this.router.navigate(['/profile']);
        });
      }
    
  }
}
