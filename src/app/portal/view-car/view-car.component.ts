import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ViewCarService} from './view-car.service';
@Component({
  selector: 'app-view-car',
  templateUrl: './view-car.component.html',
  styleUrls: ['./view-car.component.css']
})
export class ViewCarComponent implements OnInit {
  addcars = <any>[];
  constructor(private router: Router, private viewcarservice: ViewCarService) { }

  ngOnInit() {
    this.viewallcars();
  }
  viewallcars() {
  
    this.viewcarservice.viewallcars().subscribe((data: {}) => {
      console.log(data);
      this.addcars = data;
    });
  }

}
