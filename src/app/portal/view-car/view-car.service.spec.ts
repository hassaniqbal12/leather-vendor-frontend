import { TestBed } from '@angular/core/testing';

import { ViewCarService } from './view-car.service';

describe('ViewCarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewCarService = TestBed.get(ViewCarService);
    expect(service).toBeTruthy();
  });
});
