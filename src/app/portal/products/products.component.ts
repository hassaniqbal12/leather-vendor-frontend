import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { ProductsService } from './products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  constructor(public fb:FormBuilder,private toastr:ToastrService,private router:Router,private products:ProductsService) { }
  addproductform = this.fb.group({
    productname: ['', Validators.required ],
    producttype: ['', Validators.required ],
    category: ['', Validators.required ],
    featureimg: ['', Validators.required ],
    price: ['', Validators.required ],
    sku: ['', Validators.required ],
    quantity: ['', Validators.required ],
  });
  ngOnInit( ) {
  }
  productname = new FormControl('');
  producttype = new FormControl('');
  category = new FormControl('');
  featureimg = new FormControl('');
  price = new FormControl('');
  sku = new FormControl('');
  quantity = new FormControl('');
  addproduct(){
    if(this.addproductform.valid){
      this.products.addproducts(this.addproductform.value)
      .subscribe( data => {
        console.log(data);
        this.toastr.success("Product Added Sucessfully")
        this.router.navigate(['/products']);
      });
    }
  
}
}
