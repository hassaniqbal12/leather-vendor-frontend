import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProductsModel } from './ProductsModel';
@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  baseurl: string = "http://localhost:3000/api/";
  constructor(private http:HttpClient) { }
  addproducts(addproductss:ProductsModel){
    return this.http.post(this.baseurl + 'products', addproductss);
  }
}
