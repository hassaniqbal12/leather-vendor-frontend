export class ProductsModel {
    id: string;
    productname: String;
    producttype: String;
    category: String;
    featureimg: number;
    price: string;
    sku: Date;
    quantity: Date;
}