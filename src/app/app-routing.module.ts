import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './authentication/register/register.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { LoginComponent } from './authentication/login/login.component';
import { ProfileComponent } from './portal/profile/profile.component';
import { AddCarComponent } from './portal/add-car/add-car.component';
import { ViewCarComponent } from './portal/view-car/view-car.component';
import { AddDriverComponent } from './portal/add-driver/add-driver.component';
import { ViewDriverComponent } from './portal/view-driver/view-driver.component';
import { ProductsComponent } from './portal/products/products.component';
import { ForgetPasswordComponent } from './authentication/forget-password/forget-password.component';
import { ForgetEmailComponent } from './authentication/forget-email/forget-email.component';
const route: Routes = [
  {path: 'register',component: RegisterComponent},
  {path: 'login',component: LoginComponent},
  {path:'profile',component:ProfileComponent},
  {path:'addcar',component:AddCarComponent},
  {path:'viewcars',component:ViewCarComponent},
  {path:'addriver',component:AddDriverComponent},
  {path:'viewdriver',component:ViewDriverComponent },
  {path:'forgetpassword',component:ForgetPasswordComponent},
  {path:'findemail',component:ForgetEmailComponent},
  {path:'products',component:ProductsComponent},
  {

    path: '',
   redirectTo: '/register',
   pathMatch: 'full'
 },
   { path: '**', component: PagenotfoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(route)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
